#include "syncImg.h"
#include <cv.h>

SyncImg::SyncImg() : outputSize(720, 576), outputBothSize(1440, 576){
	ROS_INFO("Initialize SyncImg algorithm");
}

SyncImg::~SyncImg() {
	ROS_INFO("SyncImg algorithm successfully ended\n");
		cv::destroyWindow("view");
}

void SyncImg::imageCallback(const sensor_msgs::ImageConstPtr& msg) {
	//ROS_INFO("Entering imageCallback");

	try {
		cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);
		//cv::waitKey(30);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
	//ROS_INFO("Leaving imageCallback");
}

void SyncImg::navCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  ROS_INFO("Seq: [%d]", msg->header.seq);
  ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
  ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
  ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", msg->twist.twist.linear.x,msg->twist.twist.angular.z);
}

void SyncImg::syncImgNavCallback(const sensor_msgs::ImageConstPtr& imgMsg, const nav_msgs::Odometry::ConstPtr& navMsg) {
	try {
		std::vector<cv::Mat> mats;
		cv::Mat img(cv_bridge::toCvShare(imgMsg, "bgr8")->image);

		if (not bufferImg.empty()) {
				//visualizeImages("view", img, bufferImg);
				matchOrb(img, bufferImg);
		}
		img.copyTo(bufferImg);
		//cv::waitKey(30);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", imgMsg->encoding.c_str());
	}
  ROS_INFO("Seq: [%d]", navMsg->header.seq);
  ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", navMsg->pose.pose.position.x,navMsg->pose.pose.position.y, navMsg->pose.pose.position.z);
  ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", navMsg->pose.pose.orientation.x, navMsg->pose.pose.orientation.y, navMsg->pose.pose.orientation.z, navMsg->pose.pose.orientation.w);
  ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", navMsg->twist.twist.linear.x,navMsg->twist.twist.angular.z);
}

void SyncImg::matchOrb(cv::Mat img1, cv::Mat img2) {
	cv::ORB orb(2000);
	cv::DescriptorExtractor * extractor = new cv::OrbDescriptorExtractor();
	cv::Mat desc1, desc2;
	std::vector<cv::KeyPoint> kp1, kp2;

	//cv::cvtColor(img1, img1, CV_BGR2GRAY);
	//cv::cvtColor(img2, img2, CV_BGR2GRAY);


	orb(img1, cv::Mat(), kp1, desc1);
	orb(img2, cv::Mat(), kp2, desc2);

	desc1.convertTo(desc1, CV_32F);
	desc2.convertTo(desc2, CV_32F);
	cv::FlannBasedMatcher matcher;
	std::vector< cv::DMatch > matches;
	matcher.match( desc1, desc2, matches );

	double max_dist = 0; double min_dist = 100;


	for( int i = 0; i < desc1.rows; i++ ) { 
		double dist = matches[i].distance;
		if( dist < min_dist ) min_dist = dist;
		if( dist > max_dist ) max_dist = dist;
	}
	ROS_INFO("Distances Min: %f, Max: %f", min_dist, max_dist);
	std::vector< cv::DMatch > good_matches;

  for( int i = 0; i < desc1.rows; i++ )
  { if( matches[i].distance <= cv::max(2*min_dist, 0.02) )
    { good_matches.push_back( matches[i]); }
  }
	cv::Mat img_matches;
	cv::drawMatches( img1, kp1, img2, kp2,
               good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
               std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

  //-- Show detected matches
	cv::resize(img_matches, img_matches, outputBothSize, 0, 0, cv::INTER_CUBIC);
	cv::imshow( "view", img_matches );
	/*
		 boost::shared_ptr<cv::FlannBasedMatcher> matcherPtr;
		 cv::Ptr<cv::flann::IndexParams> indexParams = new cv::flann::LshIndexParams(12, 20, 2);
		 m_matcherPtr = boost::shared_ptr<cv::FlannBasedMatcher>(new cv::FlannBasedMatcher(indexParams));
		 m_matcherPtr->add(m_descriptorsVec);
		 m_matcherPtr->train();

	// This step takes very long
	m_matcherPtr->knnMatch(*query, knnmatches, 1);
	*/
}

void SyncImg::visualizeImages(std::string windowname, cv::Mat img1, cv::Mat img2) {
	cv::resize(img1, img1, outputSize, 0, 0, cv::INTER_CUBIC);
	cv::resize(img2, img2, outputSize, 0, 0, cv::INTER_CUBIC);
	cv::Size sz1 = img1.size();
	cv::Size sz2 = img2.size();
	cv::Mat resImg(sz1.height, sz1.width + sz2.width, CV_8UC3);
	cv::Mat left(resImg, cv::Rect(0, 0, sz1.width, sz1.height));
	img1.copyTo(left);
	cv::Mat right(resImg, cv::Rect(sz1.width, 0, sz2.width, sz2.height));
	img2.copyTo(right);
	cv::imshow(windowname, resImg);
}

bool SyncImg::init() {
	bool ret = true;
	cv::namedWindow("view");
	cv::startWindowThread();

	image_transport::ImageTransport it(nh);
	//navSub = nh.subscribe("/sensors/applanix/gps_odom", 10, &SyncImg::navCallback, this);
	image_transport::TransportHints const th("compressed");
	//sub = it.subscribe("/sensors/iavcam_right/image_raw", 10, &SyncImg::imageCallback, this, th);

	imgSubFilter.subscribe(it, "/sensors/iavcam_right/image_raw", 10, th);
	odomSub.subscribe(nh, "/sensors/applanix/gps_odom", 10);

	mSync.reset(new message_filters::Synchronizer<SyncPolicy>(SyncPolicy(5), imgSubFilter, odomSub));
	mSync->registerCallback(boost::bind(&SyncImg::syncImgNavCallback, this, _1, _2));

	ROS_INFO("Init ready");

	return true;
}


int main(int argc, char** argv)
{
	// init node
	ros::init(argc, argv, "sync_images" );

	// create instance
	SyncImg *syncImg = new SyncImg();
	if(syncImg->init())
	{

		ros::spin();
	}

	return 0;
}
