#pragma once

#include <boost/version.hpp>
#if ((BOOST_VERSION / 100) % 1000) >= 53
#include <boost/thread/lock_guard.hpp>
#endif


#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <image_transport/transport_hints.h>
#include <image_transport/subscriber_filter.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

class SyncImg {
	public:
		SyncImg();
		~SyncImg();
		bool init();
		void imageCallback(const sensor_msgs::ImageConstPtr& msg);
		void navCallback(const nav_msgs::Odometry::ConstPtr& msg);
		void syncImgNavCallback(const sensor_msgs::ImageConstPtr& imgMsg, const nav_msgs::Odometry::ConstPtr& navMsg);
		void matchOrb(cv::Mat img1, cv::Mat img2);

	private:
		void callback();
		ros::NodeHandle nh;
		ros::Subscriber navSub;
		image_transport::Subscriber sub;
		cv::Size outputSize;
		cv::Size outputBothSize;

		image_transport::SubscriberFilter imgSubFilter;
		message_filters::Subscriber<nav_msgs::Odometry> odomSub;

		typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, nav_msgs::Odometry> SyncPolicy;
		boost::shared_ptr< message_filters::Synchronizer<SyncPolicy> > mSync;
		cv::Mat bufferImg;

		void visualizeImages(std::string windowname, cv::Mat img1, cv::Mat img2);
};
